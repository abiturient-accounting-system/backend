# Abiturient Accouting System: Backend

This is Git-repository of Abiturient Accouting System (AAS) REST API. AAS is an web-application for universities to account abiturients (entrants) during the admission campaign, handle the admission's routine work and so on.

This repository includes the AAS Backend source code ([code/](code/)) and some deployment features, such as docker compose configuration, including PostgreSQL + Django + uWSGI + Nginx stack.

## Testing

To run tests:

1. Execute `docker-compose -f .deploy/docker-compose/dev.yml build`
2. Execute `docker-compose -f .deploy/docker-compose/dev.yml run django potry run python3 manage.py test`


## Deployment

To deploy project:

1. Execute `docker-compose -f .deploy/docker-compose/prod.yml build`
2. Execute `docker-compose -f .deploy/docker-compose/prod.yml up`
3. Visit [api.localhost](http://api.localhost "API URL")
