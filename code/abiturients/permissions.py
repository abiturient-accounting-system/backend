from general.permissions import BaseModelAccessPermission


class AbiturientAccessPermission(BaseModelAccessPermission):
    """Access permission for Abiturient views"""

    model_label = 'abiturient'


class GuardianAccessPermission(BaseModelAccessPermission):
    """Access permissions for Guradian views"""

    model_label = 'guardian'
