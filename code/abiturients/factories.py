"""abiturients application FactoryBoy implementation

This module contains factories to craete Abiturient, Guardian
Django model instances.
"""

import factory


class AbiturientFactory(factory.django.DjangoModelFactory):
    """Abiturient Factory

    Creates an Abiturient Django model instance. Uses Django
    model managers' .get_or_create() method on Abiturient
    first_name, last_name fields.
    """

    class Meta:
        model = 'abiturients.Abiturient'
        django_get_or_create = ('first_name', 'last_name')

    first_name = 'John'
    last_name = 'Johnson'
    phone = '+380000000000'
    email = factory.LazyAttribute(
        lambda obj: f'{obj.first_name}{obj.last_name}@gmail.com'
    )


class GuardianFactory(factory.django.DjangoModelFactory):
    """Guardian Factory

    Creates a Guardian Django model instance. Uses Django
    model manager's .get_or_create() menthod on Guardian's
    first_name, last_name fields.
    """

    class Meta:
        model = 'abiturients.Guardian'
        django_get_or_create = ('abiturient', 'family_ties')

    first_name = 'John'
    last_name = 'Doe'
    family_ties = 'Father'
    phone = '+380000000000'
    abiturients = factory.SubFactory(AbiturientFactory)
    email = factory.LazyAttribute(
        lambda obj: f'{obj.first_name}{obj.last_name}@gmail.com'
    )
