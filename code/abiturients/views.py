from django.shortcuts import get_object_or_404

from rest_framework import filters
from rest_framework import viewsets
from rest_framework.generics import ListAPIView

from general.mixins.view import (
    ViewsetActionSerializerMixin, AbiturientRelatedQuerysetMixin
)

from applications.serializers.abiturient_nested import (
    ApplicationAbiturientNestedSerializer
)

from abiturients.models import Abiturient
from abiturients.permissions import (
    AbiturientAccessPermission,
    GuardianAccessPermission
)
from abiturients.serializers.guardian import GuardianSerializer
from abiturients.serializers.abiturient import (
    AbiturientListSerializer,
    AbiturientRetrieveSerializer,
    AbiturientCreateUpdateSerializer
)


class AbiturientViewset(ViewsetActionSerializerMixin, viewsets.ModelViewSet):
    """Abiturient ModelViewSet subclass"""
    permission_classes = (AbiturientAccessPermission,)

    list_serializer_class = AbiturientListSerializer
    retrieve_serializer_class = AbiturientRetrieveSerializer

    # serializer_class is used by default for all other viewset actions
    # such as create, update, partial upadte, destroy etc.
    serializer_class = AbiturientCreateUpdateSerializer

    def get_queryset(self):
        """Returns all of abiturient objects. If ordering=status was
        specified - orders QuerySet by abiturient status
        (.get_status())
        """

        queryset = Abiturient.objects.all()

        if self.request.query_params.get('ordering', None):
            ordering = self.request.query_params['ordering']
            if ordering == 'status':
                queryset = sorted(queryset, key=lambda obj: obj.get_status())

        return queryset

    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('first_name', 'last_name', 'email')
    ordering_fields = ('first_name', 'last_name')


class AbiturientApplicationsListAPIView(
    AbiturientRelatedQuerysetMixin,
    ListAPIView
):
    """Abiturient's applications ListAPIView"""

    serializer_class = ApplicationAbiturientNestedSerializer
    related_query_field = 'applications'
    permission_classes = (AbiturientAccessPermission,)


class AbiturientGuardianViewset(
    AbiturientRelatedQuerysetMixin,
    viewsets.ModelViewSet
):
    """Guradian ModelViewSet subclass"""

    serializer_class = GuardianSerializer
    related_query_field = 'guardians'
    permission_classes = (GuardianAccessPermission,)
