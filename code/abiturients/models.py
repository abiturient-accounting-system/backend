from django.db import models

from general.models import Person

from applications.models import Application


class Abiturient(Person):
    """Abiturient database model

    Abiturient is a person, who is applying for studying
    at Unversity. Abiturient is general.Person model subclass.

    Attributes:
        first_name (str): Abiturient first name. Required.
        second_name (str): Abiturient second name (patronymic).
            Can be null
        last_name (str): Abiturient last name. Required.
        email (str): Abiturient's email
        phone (str): Abiturient's mobile phone number
        created (datetime): Instance creation date and time.
        exams (List[Exam]): List of exams abiturient has passed.
    """
    exams = models.ManyToManyField(
        'exams.Exam',
        through='exams.AbiturientExam',
        through_fields=('abiturient', 'exam'),
        related_name='abiturients'
    )

    created = models.DateTimeField(auto_now_add=True)

    class Status(models.IntegerChoices):
        """Abiturient Status enumeration

        Attributes:
            CREDITED (int): Abiturient is credited for education
            PROCESSING (int): Abiturient neither credited, nor
                regected for crediting.
            NOT_CREDITED (int): Admission Officer has rejected
                abiturient's application for studying
            UNTRACTED (int): Abiturient has not sent any
                application yet.
        """

        CREDITED = 1
        PROCESSING = 2
        NOT_CREDITED = 3
        UNTRACKED = 4

    def get_status(self):
        """Returns current abiturient status (Abiturient.Status value),
        depends on his applications status.
        """

        applications = self.applications.all()
        # initializing extra variable for application count
        # because of its' reusage
        applications_count = applications.count()

        # If abiturient has not sent any application -
        # his status for now is UNTRACKED
        if applications_count == 0:
            return self.Status.UNTRACKED

        # If abiturient has one of the applications approved
        # by admissions officers then his status is CREDITED
        if applications.filter(status=Application.Status.APPROVED).exists():
            return self.Status.CREDITED

        canceled = applications.filter(status=Application.Status.CANCELED)

        # If ALL of abiturient applications are canceled then
        # his status is NOT_CREDITED, untill he send new application
        if applications_count == canceled.count():
            return self.Status.NOT_CREDITED

        # If none of those statesments then abiturient is
        # currently processing by admission officers (he
        # has application, that has only NEW status)
        return self.Status.PROCESSING


class Guardian(Person):
    """Abiturient's guardian model

    Guradian is a person, who is responsible for abiturient before
    he gets the age of majority (his father, mather, grandparents etc).

    Attributes:
        first_name (str): Guardian first name. Required.
        second_name (str): Guardian second name (patronymic).
            Can be null
        last_name (str): Guardian last name. Required.
        email (str): Guardian's email
        phone (str): Guardian's mobile phone number
        family_ties (str): Relation to abiturient (father,
            mother etc).
        abiturient (Abiturient): Abiturient that this person is
            guardian of.
    """

    family_ties = models.CharField(max_length=100, blank=False, default=None)
    abiturient = models.ForeignKey(
        Abiturient,
        on_delete=models.CASCADE,
        related_name='guardians'
    )

    class Meta:
        unique_together = ('abiturient', 'family_ties')
