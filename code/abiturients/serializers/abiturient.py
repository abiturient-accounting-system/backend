"""Abiturient Serializers Modules

This module include ModelSerializer subclasses, that
relates to Abiturient Django model.
"""

from rest_framework import serializers

from applications.serializers.document_scan import DocumentScanNestedSerializer
from applications.serializers.abiturient_nested import (
    ApplicationAbiturientNestedSerializer
)

from exams.serializers.abiturient_exam import AbiturientExamNestedSerializer

from certificates.serializers.certificate import (
    CertificateAbiturientNestedSerializer
)

from abiturients.models import Abiturient
from abiturients.serializers.guardian import GuardianNestedSerializer


class AbiturientListSerializer(serializers.ModelSerializer):
    """Abiturient List Serializer

    This serializers represents list of abiturients and it's
    designed to be used in .list() views actions

    Attributes:
        full_name (str): Abiturient full name (a composition of
            abiturients first, second and last names)
        status (int): Const values, representing wheather abiturient
            is credited in University or not
    """
    full_name = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = Abiturient
        fields = ('id', 'full_name', 'status')

    def get_full_name(self, obj):
        return obj.get_full_name()

    def get_status(self, obj):
        return obj.get_status()


class AbiturientRetrieveSerializer(serializers.ModelSerializer):
    """Abitureint Retrieve Serializer

    This serializer class is desing to be used during .retrieve()
    views' action handling. It contains information about all of
    Abiturient Django model related models.

    Attributes:
        abiturient_exams (List[AbitureintExam]): List of exam
            results, that abiturient has passed
        certificates (List[Certificate]): List of abiturient
            certificates, he has recieved after graduating from
            other institutions
        document_scans (List[DocuemtnScan]): List of abiturient's
            document scans
        guardians (List[Guardian]): List of abiturient's guardians,
            containing information about their contacts
        applications (List[Application]): List of abiturient's
            applications
    """
    abiturient_exams = AbiturientExamNestedSerializer(many=True)
    certificates = CertificateAbiturientNestedSerializer(many=True)
    document_scans = DocumentScanNestedSerializer(many=True)
    guardians = GuardianNestedSerializer(many=True)
    applications = ApplicationAbiturientNestedSerializer(many=True)

    class Meta:
        model = Abiturient
        fields = (
            'id',
            'first_name',
            'second_name',
            'last_name',
            'email',
            'phone',
            'guardians',
            'abiturient_exams',
            'document_scans',
            'certificates',
            'applications'
        )


class AbiturientCreateUpdateSerializer(serializers.ModelSerializer):
    """Abiturient Create/Update Serializer

    This serializer is used only for creating/editing abiturient data.
    Write only.
    """
    class Meta:
        model = Abiturient
        fields = (
            'id',
            'first_name',
            'second_name',
            'last_name',
            'email',
            'phone'
        )


class AbiturientApplicationNestedSerializer(serializers.ModelSerializer):
    """Abiturient Serializer (Application Nested)

    This serializer is design only to be used as nested serializer in
    Application's ModelSerializer subclasses.

    Attributes:
        full_name (str): A string representation of abiturient's
            first, second and last name
    """
    full_name = serializers.SerializerMethodField()

    class Meta:
        model = Abiturient
        fields = ('id', 'full_name')

    def get_full_name(self, obj):
        return obj.get_full_name()
