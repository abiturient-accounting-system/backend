"""Guardian serializers module

This module contains ModelSerializer subclasses, that
uses Guardian Django model as data source.
"""

from rest_framework import serializers

from abiturients.models import Guardian


class GuardianSerializer(serializers.ModelSerializer):
    class Meta:
        model = Guardian
        fields = (
            'id',
            'first_name',
            'second_name',
            'last_name',
            'email',
            'phone',
            'abiturient',
            'family_ties'
        )


class GuardianNestedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Guardian
        fields = (
            'id',
            'first_name',
            'second_name',
            'last_name',
            'email',
            'phone',
            'family_ties'
        )
