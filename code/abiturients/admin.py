from django.contrib import admin

from abiturients import models


admin.site.register(models.Abiturient)
admin.site.register(models.Guardian)
