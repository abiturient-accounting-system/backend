from django.urls import path, include

from rest_framework.routers import DefaultRouter

from abiturients import views


app_name = 'applications'


router = DefaultRouter()
router.register(
    r'abiturients',
    views.AbiturientViewset,
    basename='abiturient'
)
router.register(
    r'guardians',
    views.AbiturientGuardianViewset,
    basename='guradian'
)


urlpatterns = [
    path('', include(router.urls)),
    path(
        'abiturient/<int:pk>/applications/',
        views.AbiturientApplicationsListAPIView.as_view()
    ),
]
