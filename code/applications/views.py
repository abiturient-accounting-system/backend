from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import (
    RetrieveModelMixin, CreateModelMixin, UpdateModelMixin
)

from general.mixins.view import ViewsetActionSerializerMixin

from applications.models import Application, DocumentScan
from applications.permissions import (
    ApplicationAccessPermission, DocumentScanAccessPermission
)
from applications.serializers.document_scan import (
    DocumentScanCreateUpdateSerializer
)
from applications.serializers.application import (
    ApplicationCreateUpdateSerializer,
    ApplicationRetrieveSerializer
)


class DocumentScanViewset(CreateModelMixin, UpdateModelMixin, GenericViewSet):
    queryset = DocumentScan.objects.all()
    serializer_class = DocumentScanCreateUpdateSerializer
    permission_classes = (DocumentScanAccessPermission,)


class ApplicationViewset(
    ViewsetActionSerializerMixin,
    CreateModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
    GenericViewSet
):
    queryset = Application.objects.all()
    retrieve_serializer_class = ApplicationRetrieveSerializer
    serializer_class = ApplicationCreateUpdateSerializer
    permission_classes = (ApplicationAccessPermission,)

