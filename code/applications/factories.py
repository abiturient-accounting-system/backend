import factory

from applications.models.Application import StudyForm, Status


class DocumentScanFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'applications.DocumentScan'
        django_get_or_create = ('abiturient', 'name', 'year')

    name = 'certificate_scan'
    image = factory.django.ImageField()
    abiturient = factory.SubFactory('abiturients.factories.AbiturientFactory')


class ApplicationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'applications.Application'
        django_get_or_create = (
            'year', 'abiturient', 'study_form', 'status', 'speciality'
        )

    priority = 5
    study_form = StudyForm.FULL_TIME
    status = Status.NEW
    abiturient = factory.SubFactory('abiturients.factories.AbiturientFactory')
    speciality = factory.SubFactory(
        'specialities.factories.SpecialityCompleteFactory'
    )
    certificate = factory.SubFactory(
        'certificates.factories.CertificateFactory'
    )
