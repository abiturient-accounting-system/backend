from django.test import TestCase

from abiturients.factories import AbiturientFactory

from applications.serializers.document_scan import (
    DocumentScanCreateUpdateSerializer
)


class TestDocumentScanCreateUpdateSerializer(TestCase):
    """Test for Base64ImageField used in DocumentScanCreateUpdateSerializer"""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.abiturient = AbiturientFactory()

    def test_document_scan_create_update_serializer(self):
        test_data = {
            'abiturient': self.abiturient.id,
            'name': 'scan',
            'year': 2020,
            'image': 'R0lGODlhAQABAIAAAP///yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='
        }

        serializer = DocumentScanCreateUpdateSerializer(data=test_data)
        self.assertTrue(serializer.is_valid())

        document_scan = serializer.save()
        self.assertIsNotNone(document_scan.id)
