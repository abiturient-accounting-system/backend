from rest_framework import serializers

from abiturients.serializers.abiturient import (
    AbiturientApplicationNestedSerializer
)

from certificates.serializers.certificate import (
    CertificateApplicationNestedSerializer
)

from exams.serializers.abiturient_exam import AbiturientExamNestedSerializer

from specialities.serializers.speciality import SpecialityNestedSerializer

from applications.models import Application
from applications.serializers.document_scan import DocumentScanNestedSerializer


class ApplicationCreateUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = (
            'id',
            'study_form',
            'payment_form',
            'status',
            'year',
            'priority',
            'abiturient',
            'certificate',
            'speciality'
        )


class ApplicationSpecialityNestedSerializer(serializers.ModelSerializer):
    abiturient = AbiturientApplicationNestedSerializer(read_only=True)
    points = serializers.SerializerMethodField()

    class Meta:
        model = Application
        fields = (
            'id',
            'study_form',
            'payment_form',
            'status',
            'year',
            'priority',
            'abiturient',
            'points'
        )

    def get_points(self, obj):
        return obj.get_points()


class ApplicationRetrieveSerializer(serializers.ModelSerializer):
    certificate = CertificateApplicationNestedSerializer(read_only=True)
    abiturient = AbiturientApplicationNestedSerializer(read_only=True)
    speciality = SpecialityNestedSerializer(read_only=True)
    exams = AbiturientExamNestedSerializer(many=True, read_only=True)
    document_scans = DocumentScanNestedSerializer(many=True, read_only=True)
    points = serializers.SerializerMethodField()

    class Meta:
        model = Application
        fields = (
            'id',
            'study_form',
            'payment_form',
            'status',
            'year',
            'priority',
            'abiturient',
            'certificate',
            'speciality',
            'exams',
            'document_scans',
            'points'
        )

    def get_points(self, obj):
        return obj.get_points()
