from rest_framework import serializers

from certificates.serializers.certificate import (
    CertificateApplicationNestedSerializer
)

from specialities.serializers.speciality import SpecialityNestedSerializer

from applications.models import Application
from applications.serializers.document_scan import (
    DocumentScanNestedSerializer
)


class ApplicationAbiturientNestedSerializer(serializers.ModelSerializer):
    certificate = CertificateApplicationNestedSerializer(read_only=True)
    speciality = SpecialityNestedSerializer(read_only=True)
    document_scans = DocumentScanNestedSerializer(many=True, read_only=True)
    points = serializers.SerializerMethodField()

    class Meta:
        model = Application
        fields = (
            'id',
            'study_form',
            'payment_form',
            'status',
            'year',
            'priority',
            'abiturient',
            'certificate',
            'speciality',
            'document_scans',
            'points'
        )

    def get_points(self, obj):
        return obj.get_points()