from rest_framework import serializers

from drf_extra_fields.fields import Base64ImageField

from applications.models import DocumentScan


class DocumentScanCreateUpdateSerializer(serializers.ModelSerializer):
    image = Base64ImageField(required=False)

    class Meta:
        model = DocumentScan
        fields = ('id', 'name', 'year', 'abiturient', 'image')


class DocumentScanNestedSerializer(serializers.ModelSerializer):
    # image = Base64ImageField(required=True)

    class Meta:
        model = DocumentScan
        fields = ('id', 'name', 'year', 'image')
