from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

from general.utils import get_current_year


class DocumentScan(models.Model):
    """Document Scan

    Document Scan is abiturient application's assets (certificates, exams'
    results list etc).

    Attributes:
        name (str): String representation of which document scan is stored.
        year (int): The year, when the document scan was created.
        image (Image): Image file with document scan
        abiturient (Abiturient): An abitureint, who is owner of document
            scans.
    """

    name = models.CharField(max_length=100, blank=False, default=None)
    year = models.PositiveIntegerField(default=get_current_year)
    abiturient = models.ForeignKey(
        'abiturients.Abiturient',
        on_delete=models.CASCADE,
        related_name='document_scans'
    )

    class Meta:
        unique_together = ('name', 'year', 'abiturient')

    def _get_default_upload_path(self, filename):
        """Returns upload path for document scan image file depends on
        it's owner and name.
        """

        file_extension = filename.split('.')[-1]
        return f'scans/{self.abiturient}/{self.name}.{file_extension}'

    image = models.ImageField(upload_to=_get_default_upload_path)


class Application(models.Model):
    """Application model

    Application is a request that abiturient is sending to apply for
    education at university or high school.

    Attributes:
        payment_form (Application.PaymentForm): A payment method for education
        study_form (Application.StudyForm): Abiturient studying form
        status (Application.Status): Current applications status.
        year (int): A year, when application was sent
        created (int): A date and time, when Application instance was craeted.
        priority (int): An application approving priority, set by abiturient.
        abiturient (Abiturient): An application sender
        speciality (Speciality): A speciality, that abiturient applies to.
        certificate (Certificate): Am abiturient's certificate, that was
            attached to application.
    """
    class StudyForm(models.IntegerChoices):
        FULL_TIME = 1
        EXTRAMURAL = 2

    class Status(models.IntegerChoices):
        NEW = 1
        APPROVED = 2
        CANCELED = 3

    class PaymentForm(models.IntegerChoices):
        GOV = 1
        SELF = 2

    payment_form = models.IntegerField(choices=PaymentForm.choices)
    study_form = models.IntegerField(choices=StudyForm.choices)
    status = models.IntegerField(choices=Status.choices, default=Status.NEW)
    year = models.PositiveIntegerField(default=get_current_year)
    created = models.DateTimeField(auto_now_add=True)
    priority = models.PositiveIntegerField(
        validators=[
            MinValueValidator(1),
            MaxValueValidator(9)
        ]
    )
    abiturient = models.ForeignKey(
        'abiturients.Abiturient',
        on_delete=models.CASCADE,
        related_name='applications'
    )
    speciality = models.ForeignKey(
        'specialities.Speciality',
        on_delete=models.CASCADE,
        related_name='applications'
    )
    certificate = models.ForeignKey(
        'certificates.Certificate',
        on_delete=models.CASCADE,
        related_name='appliactions'
    )

    class Meta:
        unique_together = (
            ('abiturient', 'priority', 'year'),
            ('abiturient', 'speciality', 'year', 'status'),
            ('abiturient', 'speciality', 'study_form', 'status')
        )

    def get_points(self):
        """Returns the application entering points, earned by abuturient.

        The points is calculated as attached certificate mean points
        multiplied by speciality coefficient plus avarage of abiturient
        exam results multiplied by speciality exam coefficient. Speciality
        coefficient is different for every exam and ranges from 1 to 2.
        """

        speciality_exams = self.speciality.speciality_exams.values_list(
            'exam',
            'coefficient',
            named=True
        )
        abiturient_exams = self.abiturient.abiturient_exams.values_list(
            'exam',
            'points',
            named=True
        )

        exam_results = []
        for abiturient_exam in abiturient_exams:
            for speciality_exam in speciality_exams:
                if speciality_exam.exam == abiturient_exam.exam:
                    exam_results.append(
                        abiturient_exam.points * speciality_exam.coefficient
                    )

        try:
            return sum(exam_results) / len(exam_results)
        except ZeroDivisionError:
            return 0
