from general.permissions import BaseModelAccessPermission


class ApplicationAccessPermission(BaseModelAccessPermission):
    model_label = 'application'


class DocumentScanAccessPermission(BaseModelAccessPermission):
    model_label = 'document_scan'
