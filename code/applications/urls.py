from django.urls import path, include

from rest_framework.routers import DefaultRouter

from applications import views


app_name = 'applications'


router = DefaultRouter()
router.register(
    r'applications',
    views.ApplicationViewset,
    basename='application'
)
router.register(
    r'document-scans',
    views.DocumentScanViewset,
    basename='document-scan'
)


urlpatterns = [
    path('', include(router.urls))
]
