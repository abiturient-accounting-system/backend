from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

from general.utils import get_current_year


class Institution(models.Model):
    """Institution model

    Institution can be school, gymnasium, collage - a place, where abiturient
    received his certificate with which he applies for education.

    Attribues:
        name (str): A name of institution (i.e. School 8)
    """

    name = models.CharField(max_length=256, blank=False, default=None)


class Certificate(models.Model):
    """Certificate model

    A certificate is a document person receives after graduation from
    educational institution.

    Attributes:
        abiturient (Abiturient): Certificate owner
        institution (Institution): A place, where certificate was recieved.
        mean_points (float): Studying subjects' marks mean value
        year (int): A year, when certificate was recieved.
    """

    abiturient = models.ForeignKey(
        'abiturients.Abiturient',
        on_delete=models.CASCADE,
        related_name='certificates'
    )
    institution = models.ForeignKey(
        Institution,
        on_delete=models.CASCADE,
        related_name='certificates'
    )
    mean_points = models.DecimalField(
        max_digits=3,
        decimal_places=1,
        validators=[
            MinValueValidator(0.0),
            MaxValueValidator(12.01)
        ]
    )
    year = models.PositiveIntegerField(default=get_current_year)

    class Meta:
        unique_together = ('abiturient', 'institution', 'year')
