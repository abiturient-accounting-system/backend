from django.test import TestCase

from certificates.factories import CertificateFactory
from certificates.serializers.certificate import (
    CertificateCreateUpdateSerializer
)


class TestCertificateCreateUpdateSerializer(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.certificate = CertificateFactory()
        cls.abiturient = cls.certificate.abiturient

    def test_certificate_serializer_create(self):
        data = {
            'abiturient': self.abiturient.id,
            'mean_points': 10.1,
            'institution': {
                'name': 'School #8'
            }
        }

        serializer = CertificateCreateUpdateSerializer(data=data)
        self.assertTrue(serializer.is_valid())

        instance = serializer.save()
        self.assertIsNotNone(getattr(instance, 'id', None))

    def test_certificate_serializer_update(self):
        data = {
            'id': self.certificate.id,
            'abiturient': self.certificate.abiturient.id,
            'mean_points': 10.1,
            'institution': {
                'name': 'School #10'
            }
        }

        serializer = CertificateCreateUpdateSerializer(data=data)
        self.assertTrue(serializer.is_valid())

        instance = serializer.save()
        self.assertEqual(instance.institution.name, 'School #10')

    def test_certificate_serializer_partial_update(self):
        data = {
            'id': self.certificate.id,
            'institution': {
                'name': '#10'
            }
        }

        serializer = CertificateCreateUpdateSerializer(
            self.certificate,
            data,
            partial=True
        )

        self.assertTrue(serializer.is_valid())

        instance = serializer.save()
        self.assertEqual(instance.institution.name, '#10')
