from rest_framework.generics import ListAPIView
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, UpdateModelMixin


from certificates.models import Certificate, Institution
from certificates.permissions import CertificateAccessPermission
from certificates.serializers.institution import InstitutionSerializer
from certificates.serializers.certificate import (
    CertificateCreateUpdateSerializer
)


class InstitutionAPIView(ListAPIView):
    queryset = Institution.objects.all()
    serializer_class = InstitutionSerializer


class CertificateViewset(CreateModelMixin, UpdateModelMixin, GenericViewSet):
    queryset = Certificate.objects.all()
    serializer_class = CertificateCreateUpdateSerializer
    permission_classes = (CertificateAccessPermission,)
