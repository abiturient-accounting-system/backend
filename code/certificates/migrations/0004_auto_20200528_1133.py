# Generated by Django 3.0.6 on 2020-05-28 11:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('certificates', '0003_auto_20200524_0843'),
    ]

    operations = [
        migrations.AlterField(
            model_name='institution',
            name='name',
            field=models.CharField(default=None, max_length=256),
        ),
    ]
