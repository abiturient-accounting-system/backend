from general.permissions import BaseModelAccessPermission


class CertificateAccessPermission(BaseModelAccessPermission):
    model_label = 'certificate'
