import factory


class InstitutionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'certificates.Institution'
        django_get_or_create = ('name',)

    name = 'School #8'


class CertificateFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'certificates.Certificate'
        django_get_or_create = ('abiturient', 'year', 'institution')

    institution = factory.SubFactory(InstitutionFactory)
    abiturient = factory.SubFactory('abiturients.factories.AbiturientFactory')
    mean_points = 10.1
    year = 2020
