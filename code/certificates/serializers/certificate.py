from rest_framework import serializers

from certificates.models import Certificate, Institution
from certificates.serializers.institution import InstitutionSerializer


class CertificateCreateUpdateSerializer(serializers.ModelSerializer):
    institution = InstitutionSerializer()

    class Meta:
        model = Certificate
        fields = (
            'id',
            'abiturient',
            'institution',
            'year',
            'mean_points'
        )

    def create(self, validated_data):
        """Works as usual .create() serializer method, but craetes
        a nested Institution instance.
        """

        institution_data = validated_data.pop('institution')

        institution, created = Institution.objects.get_or_create(
            **institution_data
        )

        certificate, created = Certificate.objects.get_or_create(
            institution=institution,
            **validated_data
        )

        return certificate

    def update(self, instance, validated_data):
        """Works as usual .update() serializer method, but craetes
        a nested Institution instance.
        """

        institution_data = validated_data.pop('institution')

        institution, created = Institution.objects.get_or_create(
            **institution_data
        )

        instance.institution = institution
        return super().update(instance, validated_data)

    def partial_update(self, instance, validated_data):
        """Works as usual .partial_update() serializer method, but craetes
        a nested Institution instance.
        """

        institution_data = validated_data.pop('institution', None)

        if not institution_data:
            return super().update(instance, validated_data)

        institution, created = Institution.objects.get_or_create(
            **institution_data
        )

        instance.institution = institution
        return super().partial_update(instance, validated_data)


class CertificateApplicationNestedSerializer(serializers.ModelSerializer):
    institution = InstitutionSerializer(read_only=True)

    class Meta:
        model = Certificate
        fields = (
            'id',
            'abiturient',
            'institution',
            'year',
            'mean_points'
        )


class CertificateAbiturientNestedSerializer(serializers.ModelSerializer):
    institution = InstitutionSerializer(read_only=True)

    class Meta:
        model = Certificate
        fields = ('id', 'institution', 'year', 'mean_points')
