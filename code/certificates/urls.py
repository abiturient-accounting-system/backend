from django.urls import path, include

from rest_framework.routers import DefaultRouter

from certificates import views


app_name = 'certificates'


router = DefaultRouter()
router.register(
    r'certificates',
    views.CertificateViewset,
    basename='certificate'
)


urlpatterns = [
    path(
        'institutions/',
        views.InstitutionAPIView.as_view(),
        name='institutions-list'
    ),
    path('', include(router.urls)),
]
