from django.urls import path

from admissions.views import OperatorAPIView, OperatorChangePasswordAPIView

app_name = 'admissions'


urlpatterns = [
    path(
        'operators/<int:pk>/',
        OperatorAPIView.as_view(),
        name='operators-detail'
    ),
    path(
        'operators/<int:pk>/password/',
        OperatorChangePasswordAPIView.as_view(),
        name='operators-password'
    )
]
