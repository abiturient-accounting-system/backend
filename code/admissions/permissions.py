from rest_framework.permissions import BasePermission

from rolepermissions.checkers import has_object_permission
from rolepermissions.permissions import register_object_checker


@register_object_checker()
def is_account_data_owner(role, user, account):
    """Checks if current user logged in is account owner"""
    return user.id == account.id


class IsAccountDataOwner(BasePermission):
    """Grants permission if current user is account owner."""

    def has_permission(self, request, view):
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        return has_object_permission(
            'is_account_data_owner',
            request.user,
            obj
        )
