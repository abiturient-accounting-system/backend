from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from general.models import Person

from admissions.managers import OperatorManager


class Operator(Person, AbstractBaseUser, PermissionsMixin):
    """Operator model

    Operator is a person who operates with this application. Operator can be
    admissions officer or university deparment worker.

    Attributes:
        email (str): Operator's email, that is used as USERNAME_FIELD on
            account authorization.
        password (str): Operator's password that is used on account
            authorization.
        is_active (bool): Boolean state of user. If user is not active, he
            can not access the application and sign in it.
        is_admin (bool): Boolean state of admin. If user is admin, he has
            all the avalible permissions.
        objects (OperatorManager): BaseUserManager implementation for user
            creation.
        department (int): The operator place of employment (admissions or
            deans office)
    """
    email = models.EmailField(
        max_length=128,
        unique=True,
        blank=False,
        default=None
    )
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = OperatorManager()

    class Department(models.IntegerChoices):
        """Operator's deparment enumeration

        Attributes:
            ADDMISSIONS_OFFICE (int): [1] The current user is employed at
            admissions officer
            DEAN_OFFICE (int): [2] The current user is employed at one of
            the university's departments
        """

        ADMISSIONS_OFFICE = 1
        DEAN_OFFICE = 2

    department = models.IntegerField(choices=Department.choices)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('department', 'first_name', 'last_name')

    @property
    def is_staff(self):
        return self.is_admin
