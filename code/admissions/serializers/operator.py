"""Operator django model serializers module"""

from rest_framework import serializers

from rolepermissions.permissions import available_perm_status

from admissions.models import Operator


class OperatorSerializer(serializers.ModelSerializer):
    """Default OperatorSerializer

    This serializer is used for all needed operator actions (create,
    update, retrieve).
    """

    permissions = serializers.SerializerMethodField()

    class Meta:
        model = Operator
        fields = (
            'id',
            'first_name',
            'second_name',
            'last_name',
            'email',
            'phone',
            'department',
            'permissions'
        )

    def get_permissions(self, obj):
        """Gets the list of user's django-role-permission permissions"""
        return list(available_perm_status(obj))


class OperatorPasswordUpdateSerializer(serializers.ModelSerializer):
    """Operator password update serializer

    This serializer is used during password changing. Validates new user
    password and resets it if the password was valid.

    Attributes:
        id (int): Operator instance id
        old_password (str): Old Operator password
        new_password (str): Password, that should be set
        confirm_password (str): new_password confirmation
    """

    id = serializers.IntegerField(required=True)
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)
    confirm_password = serializers.CharField(required=True)

    class Meta:
        model = Operator
        fields = ('id', 'old_password', 'new_password', 'confirm_password')

    def validate(self, data):
        """Validates new password to be unique and checks the new password
        confirmation. Returns data, if validation is passed. If validation
        is not passed - raises serializers.ValidationError.

        Args:
            data (dict): A data, that should be validated
        """
        operator_id = data.get('id')

        if not operator_id:
            raise serializers.ValidationError(
                'You must specify operator id'
            )

        if not Operator.objects.filter(id=operator_id).exists():
            raise serializers.ValidationError(
                f'Operator with id {operator_id} not found'
            )

        operator = Operator.objects.get(id=operator_id)

        if not operator.check_password(data.get('old_password')):
            raise serializers.ValidationError({
                'old_password': 'Old password do not match'
            })

        if data.get('new_password') != data.get('confirm_password'):
            raise serializers.ValidationError({
                'confirm_password': 'Password confirmation do not match'
            })

        return data

    def create(self, validated_data):
        """Calls when the serializer is firsly saved. Updated the user
        password.
        """

        return self._change_password(
            validated_data['id'],
            validated_data['new_password']
        )

    def update(self, instance, validated_data):
        """Calls on serializer's instance updates. Changes the user current
        password.
        """

        return self._change_password(
            validated_data['id'],
            validated_data['new_password']
        )

    def _change_password(self, operator_id: int, password: str):
        """Changes current user (serializer's model instance) password."""

        operator = Operator.objects.get(id=operator_id)
        operator.set_password(password)
        operator.save()
        return operator
