"""General application permissions testing module"""

from django.urls import path, include

from rest_framework.viewsets import ModelViewSet
from rest_framework.routers import DefaultRouter
from rest_framework.test import (
    APITestCase,
    URLPatternsTestCase
)

from admissions.models import Operator
from admissions.serializers.operator import OperatorSerializer
from admissions.factories import OperatorFactory
from admissions.permissions import IsAccountDataOwner


class OperatorSampleViewset(ModelViewSet):
    """A sample view, used only for testing purposes"""

    queryset = Operator.objects.all()
    serializer_class = OperatorSerializer
    permission_classes = (IsAccountDataOwner,)


# DefaultRouter to create URL endpoints of OperatorSampleViewset
router = DefaultRouter()
router.register(r'ops', OperatorSampleViewset, basename='op')


class TestIsAccountDataOwnerPermission (APITestCase, URLPatternsTestCase):
    """Test class for BaseAccessPermission subclass"""

    urlpatterns = [
        path('', include(router.urls))
    ]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.operator = OperatorFactory()
        cls.another_operator = OperatorFactory(
            first_name='Leonid',
            last_name='Leonov'
        )

    def test_is_account_data_owner_has_permission(self):
        self.client.force_authenticate(user=self.operator)

        response = self.client.get('/ops/')

        self.assertEqual(response.status_code, 200)

    def test_is_account_data_owner_has_permission_not_authenticated(self):
        response = self.client.get('/ops/')

        self.assertEqual(response.status_code, 401)

    def test_is_account_data_owner_has_object_permission(self):
        self.client.force_authenticate(user=self.operator)

        response = self.client.get(f'/ops/{self.operator.id}/')

        self.assertEqual(response.status_code, 200)

    def test_is_account_data_owner_has_object_permission_another_user(self):
        self.client.force_authenticate(user=self.another_operator)

        response = self.client.get(f'/ops/{self.operator.id}/')

        self.assertEqual(response.status_code, 403)

    def test_is_account_data_owner_has_object_permission_not_authenticated(
        self
    ):
        response = self.client.get(f'/ops/{self.operator.id}/')

        self.assertEqual(response.status_code, 401)
