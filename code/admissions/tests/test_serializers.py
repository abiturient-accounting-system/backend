"""ModelSerializer's Testing module"""

from django.test import TestCase

from admissions.factories import OperatorFactory
from admissions.serializers.operator import OperatorPasswordUpdateSerializer


class TestOperatorPasswordUpdateSerializer(TestCase):
    """OperatorPasswordUpdateSerializer testing class"""

    def setUp(self):
        self.operator = OperatorFactory()
        self.password = '123'
        self.operator.set_password(self.password)
        self.operator.save()

    def test_password_change(self):
        data = {
            'id': self.operator.id,
            'old_password': self.password,
            'new_password': '321',
            'confirm_password': '321'
        }

        serializer = OperatorPasswordUpdateSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        operator = serializer.save()
        self.assertTrue(operator.check_password('321'))

    def test_password_change_confirm_validation_failed(self):
        data = {
            'id': self.operator.id,
            'old_password': self.password,
            'new_password': '321',
            'confirm_password': '123'
        }

        serializer = OperatorPasswordUpdateSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('confirm_password', serializer.errors)

    def test_password_change_old_password_validation_failed(self):
        data = {
            'id': self.operator.id,
            'old_password': self.password + 'a',
            'new_password': '321',
            'confirm_password': '321'
        }

        serializer = OperatorPasswordUpdateSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('old_password', serializer.errors)
