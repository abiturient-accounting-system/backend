"""admissions application FactoryBoy module"""

import factory

from admissions.models import Operator


class UserModelFactory(factory.django.DjangoModelFactory):
    """User Model Factory

    Works as DjangoModelFactory, but refreshes the user data on create to
    fetch the changes were made on post_save signal handling (usually, the user
    roles and groups are set in signal handler).
    """

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        manager = cls._get_manager(model_class)
        if cls._meta.django_get_or_create:
            instance = cls._get_or_create(model_class, *args, **kwargs)
        else:
            instance = manager.create_user(*args, **kwargs)
        return manager.get(id=instance.id)


class OperatorFactory(UserModelFactory):
    """Operator Django model factory

    Creates Operator model instance. Uses Django model managers'
    .get_or_create() method on object creation with instance .email
    attribute (becouse of it's unique constraint).
    """
    class Meta:
        model = 'admissions.Operator'
        django_get_or_create = ('email',)

    first_name = 'Ivan'
    second_name = 'Ivanovich'
    last_name = 'Ivanov'
    phone = '+380000000000'
    email = factory.LazyAttribute(
        lambda obj: f'{obj.first_name}{obj.last_name}@gmail.com'
    )
    department = Operator.Department.ADMISSIONS_OFFICE
