from django.dispatch import receiver
from django.db.models.signals import post_save

from rest_framework.authtoken.models import Token

from rolepermissions.roles import assign_role

from admissions.models import Operator


@receiver(post_save, sender=Operator)
def handle_operator_post_save(instance=None, created=False, **kwargs):
    """Calls on Operator post_save signal. If the new object is greated -
    assigns it to opeartor's default role to grant all of operator's
    permissions.
    """

    if created:
        assign_role(instance, 'default_operator')
        Token.objects.create(user=instance)
