from django.contrib.auth.models import BaseUserManager


class OperatorManager(BaseUserManager):
    """Operator Django model manager.

    This class implements the BaseUserManager class with .create_user()
    and .create_superuser() methods.
    """

    def create_user(self, email, department, first_name, last_name,
                    password=None):
        """Creates and saves a User with the given email, deparment
        and password.
        """

        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            department=department,
            first_name=first_name,
            last_name=last_name
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, department, first_name, last_name,
                         password=None):
        """Creates and saves a superuser with the given email, date of
        birth and password.
        """

        user = self.create_user(
            email,
            password=password,
            department=department,
            first_name=first_name,
            last_name=last_name
        )
        user.is_admin = True
        user.save(using=self._db)
        return user
