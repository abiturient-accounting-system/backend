from rest_framework.generics import GenericAPIView
from rest_framework.mixins import RetrieveModelMixin, UpdateModelMixin

from admissions.models import Operator
from admissions.permissions import IsAccountDataOwner
from admissions.serializers.operator import (
    OperatorSerializer, OperatorPasswordUpdateSerializer
)


class OperatorGenericAPIView(GenericAPIView):
    queryset = Operator.objects.all()
    permission_classes = (IsAccountDataOwner,)


class OperatorAPIView(
    RetrieveModelMixin,
    UpdateModelMixin,
    OperatorGenericAPIView
):
    serializer_class = OperatorSerializer


class OperatorChangePasswordAPIView(UpdateModelMixin, OperatorGenericAPIView):
    serializer_class = OperatorPasswordUpdateSerializer
