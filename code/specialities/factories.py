import factory

from specialities.models.Speciality import Category


class SpecialityFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'specialities.Speciality'
        django_get_or_create = ('year', 'code', 'category')

    code = 121
    name = 'Software Engineering'
    category = Category.BACHELOR_SCHOOL
    certificate_coefficient = 1.1


class SpecialityCompleteFactory(SpecialityFactory):
    speciality_exams = factory.RelatedFactory(
        'exams.factories.SpecialityExamFactory',
        'speciality'
    )
