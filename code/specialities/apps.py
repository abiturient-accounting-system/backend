from django.apps import AppConfig


class SpecialitiesConfig(AppConfig):
    name = 'specialities'
