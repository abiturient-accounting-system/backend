from django.urls import path, include

from rest_framework.routers import DefaultRouter

from specialities import views


app_name = 'specialities'


router = DefaultRouter()
router.register(
    r'specialities',
    views.SpecialityViewset,
    basename='speciality'
)


urlpatterns = [
    path('', include(router.urls)),
    path(
        'specialities/<int:pk>/applications/',
        views.SpecialityApplicationsAPIView.as_view(),
        name='specialities-applications'
    )
]
