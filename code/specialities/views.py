from rest_framework.generics import RetrieveAPIView
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin

from django_filters.rest_framework import DjangoFilterBackend

from general.mixins.view import ViewsetActionSerializerMixin

from applications.permissions import ApplicationAccessPermission
from applications.serializers.application import (
    ApplicationSpecialityNestedSerializer
)

from specialities.models import Speciality
from specialities.serializers.speciality import (
    SpecialityListSerializer,
    SpecialityRetrieveSerializer
)


class SpecialityViewset(
    ViewsetActionSerializerMixin,
    ListModelMixin,
    RetrieveModelMixin,
    GenericViewSet
):
    queryset = Speciality.objects.all()
    retrieve_serializer_class = SpecialityRetrieveSerializer
    serializer_class = SpecialityListSerializer


class SpecialityApplicationsAPIView(RetrieveAPIView):
    serializer_class = ApplicationSpecialityNestedSerializer
    permission_classes = (ApplicationAccessPermission,)
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('year', 'priority', 'study_form')

    def get_queryset(self):
        speciality = Speciality.objects.get(pk=self.kwargs['pk'])
        return speciality.applications.all()
