from rest_framework import serializers

from exams.serializers.speciality_exam import SpecialityExamNestedSerializer

from specialities.models import Speciality


class SpecialityListSerializer(serializers.ModelSerializer):
    application_count = serializers.SerializerMethodField()

    class Meta:
        model = Speciality
        fields = (
            'id',
            'code',
            'name',
            'year',
            'category',
            'application_count'
        )

    def get_application_count(self, obj):
        return obj.applications.all().count()


class SpecialityRetrieveSerializer(serializers.ModelSerializer):
    speciality_exams = SpecialityExamNestedSerializer(many=True)
    application_count = serializers.SerializerMethodField()

    class Meta:
        model = Speciality
        fields = (
            'id',
            'code',
            'name',
            'year',
            'category',
            'speciality_exams',
            'certificate_coefficient',
            'application_count'
        )

    def get_application_count(self, obj):
        return obj.applications.all().count()


class SpecialityNestedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Speciality
        fields = ('id', 'code', 'name', 'category', 'year')
