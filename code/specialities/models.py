from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

from general.utils import get_current_year


class Speciality(models.Model):
    """Speciality model

    A speciality is university studying discipline, for example Computer
    Science, that abiturient applies to.

    Attributes:
        code (int): A speciality unified code (i.e. 121)
        name (int): A speciality short name (i.e. Software Engineering)
        cetegory (int): A degree, that abituirent gets at studies finish
        year (int): A year, that speciality data is related to.
        certificate_coefficient (Decimal): A coefficient, that abiturients'
            certificate is multiplied by.
        exams (List[Exam]): A list of exams, need to apply to this speciality.
    """

    class Category(models.IntegerChoices):
        BACHELOR_SCHOOL = 1
        BACHELOR_COLLEGE = 2
        MASTER = 3

    code = models.PositiveIntegerField()
    name = models.CharField(max_length=256, blank=False, default=None)
    category = models.IntegerField(choices=Category.choices)
    year = models.PositiveIntegerField(default=get_current_year)
    certificate_coefficient = models.DecimalField(
        max_digits=2,
        decimal_places=1,
        validators=[
            MinValueValidator(0.99),
            MaxValueValidator(2.01)
        ]
    )
    exams = models.ManyToManyField(
        'exams.Exam',
        through='exams.SpecialityExam',
        through_fields=('speciality', 'exam')
    )

    class Meta:
        unique_together = ('code', 'category', 'year')

    def __str__(self):
        return f'[{self.code}] {self.name} ({self.category})'
