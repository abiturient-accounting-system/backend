from rest_framework import serializers

from exams.models import SpecialityExam
from exams.serializers.exam import ExamSerializer


class SpecialityExamNestedSerializer(serializers.ModelSerializer):
    exam = ExamSerializer(read_only=True)

    class Meta:
        model = SpecialityExam
        fields = ('id', 'exam', 'coefficient')
