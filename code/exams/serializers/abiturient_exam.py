from rest_framework import serializers

from exams.models import AbiturientExam
from exams.serializers.exam import ExamSerializer


class AbiturientExamCreateUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AbiturientExam
        fields = ('id', 'abiturient', 'exam', 'points', 'year')


class AbiturientExamNestedSerializer(serializers.ModelSerializer):
    exam = ExamSerializer(read_only=True)

    class Meta:
        model = AbiturientExam
        fields = ('id', 'exam', 'points', 'year')
