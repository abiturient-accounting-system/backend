"""Exams appliaction FactoryBoy implementations"""

import factory


class ExamFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'exams.Exam'
        django_get_or_create = ('name',)

    name = 'English'


class AbiturientExamFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'exams.AbiturientExam'
        django_get_or_create = ('exam', 'abiturient', 'year')

    points = 170
    exam = factory.SubFactory(ExamFactory)
    abiturient = factory.SubFactory('abiturients.factories.AbiturientFactory')


class SpecialityExamFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'exams.SpecialityExam'
        django_get_or_create = ('exam', 'speciality')

    coefficient = 1.3
    exam = factory.SubFactory(ExamFactory)
    speciality = factory.SubFactory('specialities.factories.SpecialityFactory')
