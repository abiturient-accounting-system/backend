from django.urls import path, include

from rest_framework.routers import DefaultRouter

from exams import views


app_name = 'exams'


router = DefaultRouter()
router.register(
    r'abiturient-exams',
    views.AbiturientExamViewset,
    basename='abiturient-exam'
)


urlpatterns = [
    path('', include(router.urls)),
    path('exams/', views.ExamAPIView.as_view(), name='exams-list'),
]
