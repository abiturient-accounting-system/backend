from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

from general.utils import get_current_year


class Exam(models.Model):
    """Exam Django model

    Exam is a final testing that abiturient have passed to apply for
    unversity studing.

    Attributes:
        name (str): An exam subject name
    """

    name = models.CharField(max_length=128, blank=False)

    def __str__(self):
        """Exam string representation. Returns exam subject name"""
        return self.name


class AbiturientExam(models.Model):
    """Abiturient exam Django model

    Abiturient exam is a through-model, stores relation between exam and
    abiturient with some extra information, such as exam results.

    Attributes:
        abiturient (Abiturient): A person, who passed the exam
        exam (Exam): An exam that was passed
        points (int): An amount of points abutyrient have recieved during
            passing the exam.
        year (int): A year, when the exam was passed
    """
    abiturient = models.ForeignKey(
        'abiturients.Abiturient',
        on_delete=models.CASCADE,
        related_name='abiturient_exams'
    )
    exam = models.ForeignKey(
        Exam,
        on_delete=models.CASCADE,
        related_name='abiturient_exams'
    )
    year = models.PositiveIntegerField(default=get_current_year)
    points = models.DecimalField(max_digits=4, decimal_places=1)

    class Meta:
        unique_together = ('abiturient', 'exam', 'year')


class SpecialityExam(models.Model):
    """Speciality exam Django model

    SpecialityExam is a through-model, storing data about Speciality and Exam
    models relation and some extra data, such as specialities' exams
    coefficient.

    Attributes:
        speciality (Speciality): A speciality object relates to
        exam (Exam): A exam object relates to
        coefficient (Decimal): An exam coefficient, that is used to calculate
            abiturient's application points when appling to speciality.
    """

    speciality = models.ForeignKey(
        'specialities.Speciality',
        on_delete=models.CASCADE,
        related_name='speciality_exams'
    )
    exam = models.ForeignKey(
        Exam,
        on_delete=models.CASCADE,
        related_name='specilaity_exams'
    )
    coefficient = models.DecimalField(
        max_digits=2,
        decimal_places=1,
        validators=[
            MinValueValidator(0.99),
            MaxValueValidator(2.01)
        ]
    )

    class Meta:
        unique_together = ('speciality', 'exam')
