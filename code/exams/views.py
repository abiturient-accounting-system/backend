from rest_framework.generics import ListCreateAPIView
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, UpdateModelMixin

from exams.models import Exam, AbiturientExam
from exams.permissions import AbiturientExamAccessPermission
from exams.serializers.exam import ExamSerializer
from exams.serializers.abiturient_exam import (
    AbiturientExamCreateUpdateSerializer
)


class ExamAPIView(ListCreateAPIView):
    queryset = Exam.objects.all()
    serializer_class = ExamSerializer


class AbiturientExamViewset(
    CreateModelMixin,
    UpdateModelMixin,
    GenericViewSet
):
    queryset = AbiturientExam.objects.all()
    serializer_class = AbiturientExamCreateUpdateSerializer
    permission_classes = (AbiturientExamAccessPermission,)

