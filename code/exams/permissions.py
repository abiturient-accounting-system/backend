from general.permissions import BaseModelAccessPermission


class AbiturientExamAccessPermission(BaseModelAccessPermission):
    model_label = 'abiturient_exam'
