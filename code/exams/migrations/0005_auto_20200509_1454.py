# Generated by Django 3.0.6 on 2020-05-09 14:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('abiturients', '0006_auto_20200509_1447'),
        ('exams', '0004_auto_20200509_1448'),
    ]

    operations = [
        migrations.AlterField(
            model_name='abiturientexam',
            name='points',
            field=models.DecimalField(decimal_places=1, max_digits=4),
        ),
        migrations.AlterUniqueTogether(
            name='abiturientexam',
            unique_together={('abiturient', 'exam', 'year')},
        ),
    ]
