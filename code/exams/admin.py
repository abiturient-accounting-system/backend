from django.contrib import admin

from exams.models import (
    Exam,
    AbiturientExam,
    SpecialityExam
)


admin.site.register(Exam)
admin.site.register(AbiturientExam)
admin.site.register(SpecialityExam)
