"""Reports Django appliaction

This module provides API for creating reports from admissions campaign work.
For example, it calculates the sum of newly created applications, or collects
ths list of most popular specialities to apply for.
"""