from rest_framework.permissions import BasePermission

from rolepermissions.checkers import has_permission


class CanAccessReport(BasePermission):
    """Permission checker for reports"""

    def has_permission(self, request, view):
        return has_permission(request.user, 'can_read_report')

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)
