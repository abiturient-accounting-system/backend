from django.urls import path

from reports import views


app_name = 'reports'


urlpatterns = [
    path(
        'specialities/popular/',
        views.SpecialityByPopularityReportAPIView.as_view(),
        name='popular-specialities'
    ),
    path(
        'applications/new/',
        views.NewApplicationCountAPIView.as_view(),
        name='new-application-count'
    )
]
