"""Reporters module

Reporter is a class, that creates particular report. For example, counts the
newly created applications.
"""

from datetime import timedelta
from django.utils import timezone

from applications.models import Application


class NewApplicationCountReporter:
    """New application counter

    This class provides simple API to get the newly created applications count.

    Args:
        period (NewApplicationCountReporter.CoutingPeriod): a period, during
            which the application is claimed as "new"
    """

    class CoutingPeriod:
        """Conting period enumeration"""

        HOUR = 1
        DAY = 2
        WEEK = 3
        MONTH = 4
        YEAR = 5
        ALL_TIME = 6

    def __init__(self, period=None):
        self.period = period or self.CoutingPeriod.HOUR

    def _get_counting_start_date(self):
        """Returns the couting period, depends on initial const value"""
        current_date = timezone.now()
        if self.period == self.CoutingPeriod.HOUR:
            return current_date - timedelta(hours=1)
        if self.period == self.CoutingPeriod.DAY:
            return current_date - timedelta(days=1)
        if self.period == self.CoutingPeriod.WEEK:
            return current_date - timedelta(days=7)
        if self.period == self.CoutingPeriod.MONTH:
            return current_date - timedelta(days=30)
        if self.period == self.CoutingPeriod.YEAR:
            return current_date - timedelta(days=365)
        return current_date.replace(day=1, month=1, year=1)

    def create_report(self):
        """Creates report meaning gets new application count"""

        couting_start_date = self._get_counting_start_date()
        new_application_count = Application.objects.filter(
            created__gte=couting_start_date
        ).count()
        return {
            'new_application_count': new_application_count
        }
