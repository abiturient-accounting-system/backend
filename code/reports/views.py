from django.db.models import Count

from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from specialities.models import Speciality
from specialities.serializers.speciality import SpecialityListSerializer

from reports.permissions import CanAccessReport
from reports.reporters import NewApplicationCountReporter


class SpecialityByPopularityReportAPIView(ListAPIView):
    """Returns the list of popular specialities

    The speciality is more popular the more applications it has.
    """

    serializer_class = SpecialityListSerializer
    permission_classes = (CanAccessReport,)

    SPECIALITY_COUNT_DEFAULT = 5

    def get_queryset(self):
        speciality_count = self.request.query_params.get('count')

        # If value was not provided as GET request parameter - set it's
        # default value
        if not speciality_count:
            speciality_count = self.SPECIALITY_COUNT_DEFAULT

        # Return specialities with biggest applications count (as the most)
        # in amount of speciality_count
        return Speciality.objects.annotate(
            application_count=Count(
                'applications'
            ),
        ).order_by(
            'application_count'
        )[:speciality_count]


class NewApplicationCountAPIView(APIView):
    """Returns the number of newly created applications."""

    permission_classes = (CanAccessReport,)

    def get(self, request, format=None):
        period = self.request.query_params.get('period', None)

        try:
            period = int(period)
        except ValueError:
            return Response(
                {'period': f'Period should be number, not {period}'},
                status=status.HTTP_400_BAD_REQUEST
            )

        reporter = NewApplicationCountReporter(period=period)
        response_data = reporter.create_report()

        return Response(
            data=response_data,
            status=status.HTTP_200_OK
        )
