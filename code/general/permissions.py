"""General base permission module

This module stores base permission classes, that is used to handle permission
checks in system.
"""

from rest_framework.permissions import BasePermission, SAFE_METHODS

from rolepermissions.checkers import has_permission


class BaseAccessPermission(BasePermission):
    """Base Permission class, composites the Django Role Permissions and
    Django REST Framework permissions' module features.

    Attributes:
        read_permission (str): A permission string representation, used while
            checking read permissions
        write_permission (str): A permission string representation, used while
            checking write permissions
        delete_permission (str): A permission string representation, used while
            checking delete permissions
    """

    def has_permission(self, request, view):
        """Checks the module level permissions."""

        if request.method in SAFE_METHODS:
            return has_permission(request.user, self.get_read_permission())
        if request.method in ['POST', 'PUT', 'PATCH']:
            return has_permission(request.user, self.get_write_permission())
        if request.method == 'DELETE':
            return has_permission(request.user, self.get_delete_permission())
        return False

    def has_object_permission(self, request, view, obj):
        """Checks the object level permissions."""

        return self.has_permission(request, view)

    def get_read_permission(self):
        """Returns the name (str) of resource reading permission."""

        return getattr(self, 'read_permission')

    def get_write_permission(self):
        """Returns the name (str) of resource writing permission."""

        return getattr(self, 'write_permission')

    def get_delete_permission(self):
        """Returns the name (str) of resource destroying permission."""

        return getattr(self, 'delete_permission')


class BaseModelAccessPermission(BaseAccessPermission):
    """BaseAccessPErmission subclass

    BaseModelAccessPermissions uses the Django model tag to check for model
    level permissions, set in Django Role Permissions .roles module.

    Attributes:
        model_label (str): A Django model label is used to check for reading,
            writing and destroying permissions. For example, if model_label
            is 'exam', the reading permission would be 'can_read_exam' and
            so on.
    """

    def get_model_label(self):
        return getattr(self, 'model_label')

    def get_read_permission(self):
        return f'can_read_{self.get_model_label()}'

    def get_write_permission(self):
        return f'can_write_{self.get_model_label()}'

    def get_delete_permission(self):
        return f'can_delete_{self.get_model_label()}'
