from django.urls import path, include

from rest_framework.routers import DefaultRouter

from rest_framework.test import (
    APITestCase,
    URLPatternsTestCase
)

from rest_framework.viewsets import ModelViewSet

from rolepermissions.roles import assign_role

from admissions.factories import OperatorFactory

from exams.models import Exam
from exams.factories import ExamFactory
from exams.serializers.exam import ExamSerializer

from general.permissions import BaseModelAccessPermission


class ExamSampleAccessPermission(BaseModelAccessPermission):
    model_label = 'exam'


class ExamSampleViewset(ModelViewSet):
    queryset = Exam.objects.all()
    serializer_class = ExamSerializer
    permission_classes = (ExamSampleAccessPermission,)


router = DefaultRouter()
router.register(
    r'exams',
    ExamSampleViewset,
    basename='exam'
)


class TestBaseModelAccessPermissions(APITestCase, URLPatternsTestCase):
    urlpatterns = [
        path('', include(router.urls))
    ]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.operator_without_permissions = OperatorFactory()
        cls.operator_with_permissions = OperatorFactory(
            first_name='Junior',
            last_name='Soprano'
        )

        assign_role(cls.operator_with_permissions, 'admin_role')

    def test_read_permission(self):
        self.client.force_authenticate(user=self.operator_with_permissions)
        response = self.client.get('/exams/')
        self.assertEqual(response.status_code, 200)

    def test_read_permission_negative(self):
        self.client.force_authenticate(user=self.operator_without_permissions)
        response = self.client.get('/exams/')
        self.assertEqual(response.status_code, 403)

    def test_write_permission(self):
        self.client.force_authenticate(user=self.operator_with_permissions)
        count_before = Exam.objects.count()
        response = self.client.post('/exams/', {
            'name': 'Ukrainian'
        })
        count_after = Exam.objects.count()
        self.assertEqual(response.status_code, 201)
        self.assertEqual(count_before + 1, count_after)

    def test_write_permission_negative(self):
        self.client.force_authenticate(user=self.operator_without_permissions)
        count_before = Exam.objects.count()
        response = self.client.post('/exams/', {
            'name': 'Ukrainian'
        })
        count_after = Exam.objects.count()
        self.assertEqual(response.status_code, 403)
        self.assertEqual(count_before, count_after)

    def test_delete_permission(self):
        exam = ExamFactory()
        self.client.force_authenticate(user=self.operator_with_permissions)
        count_before = Exam.objects.count()
        response = self.client.delete(f'/exams/{exam.id}/')
        count_after = Exam.objects.count()
        self.assertEqual(response.status_code, 204)
        self.assertEqual(count_before - 1, count_after)

    def test_delete_permission_negative(self):
        exam = ExamFactory()
        self.client.force_authenticate(user=self.operator_without_permissions)
        count_before = Exam.objects.count()
        response = self.client.delete(f'/exams/{exam.id}/')
        count_after = Exam.objects.count()
        self.assertEqual(response.status_code, 403)
        self.assertEqual(count_before, count_after)
