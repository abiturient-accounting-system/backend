from django.db import models

from phonenumber_field.modelfields import PhoneNumberField


class Person(models.Model):
    first_name = models.CharField(max_length=64, blank=False)
    second_name = models.CharField(max_length=64, blank=True)
    last_name = models.CharField(max_length=64, blank=False)
    email = models.EmailField(blank=True)
    phone = PhoneNumberField(null=True, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        if not self.second_name:
            return f'{self.first_name} {self.last_name}'
        return f'{self.first_name} {self.second_name} {self.last_name}'
