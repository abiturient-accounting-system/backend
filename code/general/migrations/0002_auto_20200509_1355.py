# Generated by Django 3.0.6 on 2020-05-09 13:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Contacts',
        ),
        migrations.DeleteModel(
            name='FullName',
        ),
    ]
