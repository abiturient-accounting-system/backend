"""Mixin, used in compare with DRF APIView"""

from abiturients.models import Abiturient, Guardian

from general.exceptions import MixinUsageException


class ViewsetActionSerializerMixin:
    """Per viewset action serializer mixin

    This mixin provides the specific attributes usage for setting the
    proper serializer_class for each viewset action.

    Attributes:
        list_serializer_class (Serializer): Used on .list()
        create_serializer_class (Serializer: 
    """
    def get_serializer_class(self):
        return (
            getattr(self, f'{self.action}_serializer_class', None)
            or super().get_serializer_class()
        )


class AbiturientRelatedQuerysetMixin:
    """Abiturient nested objects query mixin

    Modifies .get_queryset() to get abiturient from URL's pk attribute and
    gets the list of abiturient related model objects.

    Attributes:
        related_query_field (str): A name of Abiturient model field is used
            to get the list of objects. For example, value of 'applications'
            will return abiturient.applications.all() queryset
    """

    related_query_field: str = ''

    def get_queryset(self):
        if not self.related_query_field:
            raise MixinUsageException('You must specity .related_query_field')

        if not Abiturient.objects.filter(pk=self.kwargs['pk']).exists():
            return Guardian.objects.none()
        abiturient = Abiturient.objects.get(pk=self.kwargs['pk'])

        return getattr(abiturient, self.related_query_field).all()
