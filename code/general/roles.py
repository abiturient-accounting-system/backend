"""Django Role Permissions implementation module

This module stores data about user roles, that is used in this application.
"""


from rolepermissions.roles import AbstractUserRole


class AdminRole(AbstractUserRole):
    """Admin user role

    Admin user role stores data about all available permissions in systm.
    """

    available_permissions = {
        'can_read_application': True,
        'can_write_application': True,
        'can_delete_application': True,
        'can_read_abiturient': True,
        'can_write_abiturient': True,
        'can_delete_abiturient': True,
        'can_read_guardian': True,
        'can_write_guardian': True,
        'can_delete_guardian': True,
        'can_read_document_scan': True,
        'can_write_document_scan': True,
        'can_delete_document_scan': True,
        'can_read_certificate': True,
        'can_write_certificate': True,
        'can_delete_certificate': True,
        'can_read_abiturient_exam': True,
        'can_write_abiturient_exam': True,
        'can_delete_abiturient_exam': True,
        'can_read_exam': True,
        'can_write_exam': True,
        'can_delete_exam': True,
        'can_read_report': True,
        'can_create_document': True
    }


class DefaultOperator(AbstractUserRole):
    """Default Operator user role

    Stores data about operator's permissions.
    """

    available_permissions = {
        'can_read_application': True,
        'can_write_application': True,
        'can_read_abiturient': True,
        'can_write_abiturient': True,
        'can_read_guardian': True,
        'can_write_guardian': True,
        'can_read_document_scan': True,
        'can_write_document_scan': True,
        'can_read_certificate': True,
        'can_write_certificate': True,
        'can_read_abiturient_exam': True,
        'can_write_abiturient_exam': True,
        'can_create_document': True
    }
