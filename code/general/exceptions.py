"""General exception, that is raised in case program errors"""


class MixinUsageException(BaseException):
    """Raises on invalid mixin using."""
    pass
