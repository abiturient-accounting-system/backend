from django.urls import path

from rest_framework.authtoken.views import obtain_auth_token

from authentication import views


app_name = 'authentication'


urlpatterns = [
    path(
        'login/',
        obtain_auth_token,
        name='login'
    ),
    path(
        'user/',
        views.CurrentUserAPIView.as_view(),
        name='current-user'
    ),
]
