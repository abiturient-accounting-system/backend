from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from admissions.serializers.operator import OperatorSerializer


class CurrentUserAPIView(APIView):
    """Returns serialized current user data"""

    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        serializer = OperatorSerializer(request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)
