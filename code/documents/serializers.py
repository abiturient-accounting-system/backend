from rest_framework import serializers


class AbiturientContractSerializer(serializers.Serializer):
    contract_id = serializers.IntegerField(min_value=1)
    start_date = serializers.DateField()
    end_date = serializers.DateField()
    price = serializers.DecimalField(max_digits=8, decimal_places=2)
    speciality = serializers.CharField(max_length=256)
    faculty = serializers.CharField(max_length=256)
    study_form = serializers.CharField(max_length=128)
    degree = serializers.CharField(max_length=128)
