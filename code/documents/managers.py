"""Doucment Managers module

Document managers renders the needed document. All document managers are
BaseDocumentManager subclasses.
"""

from django.template.loader import render_to_string


class BaseDocumentManager:
    """Base document manager class

    BaseDocumentManager is a template-class for all document managers. It
    provides methods to render an HTML from template, using context data.

    Attributes:
        template_name (str): Path to Django's HTML template

    Args:
        abiturient (Abiturient): Abiturient instance, used in context data
    """

    template_name = None

    def __init__(self, abiturient, **kwargs):
        self.abiturient = abiturient
        self.kwargs = kwargs

    def create(self):
        """Renders HTML template to string using the context data. The
        HTML template is got from .get_template_name() and the context data is
        delivered by .get_context_data() method. It is not recommended to
        override this method.
        """

        template_name = self.get_template_name()
        context_data = self.get_context_data()
        return render_to_string(template_name, context_data)

    def get_template_name(self):
        """Returns path to Django HTMl tempalte is used to render. Override
        this method if you need to add extra logic. By default it returns
        .template_name attribute.
        """

        if not self.template_name:
            raise NotImplementedError(
                'You should specify .template_name in BaseDocumentManager '
                'subclasses in order to create documents'
            )

        return self.template_name

    def get_context_data(self):
        """Returns the context data, that is furtherly passed throw render
        function. Override this method to pass more context data in .create()
        function.
        """

        return {
            'user': self.kwargs.get('user', None),
            'abiturient': self.abiturient
        }


class AbiturientContractManager(BaseDocumentManager):
    """Manages the abiturient's educational contract creation"""

    template_name = 'documents/contract.html'

    def get_context_data(self):
        return {
            'contract_id': self.kwargs.get('contract_id', None),
            'abiturient': self.abiturient,
            'start_date': self.kwargs.get('start_date', None),
            'end_date': self.kwargs.get('end_date', None),
            'price': self.kwargs.get('price', None),
            'study_form': self.kwargs.get('study_form', None),
            'degree': self.kwargs.get('degree', None),
            'speciality': self.kwargs.get('speciality', None),
            'faculty': self.kwargs.get('faculty', None)
        }
