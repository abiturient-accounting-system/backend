from django.shortcuts import get_object_or_404

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from abiturients.models import Abiturient

from documents.permissions import DocumentCreationAccessPermission
from documents.serializers import AbiturientContractSerializer
from documents.managers import AbiturientContractManager


class DocumentCreateAPIView(APIView):
    """Base DocumentCreateView

    Attributes:
        serializer_class (Serializer): A serializer is user to validate
            POST request data.
        document_manager_class (BaseDocumentManager): A document manager is
            used to create document.
    """

    permission_classes = (DocumentCreationAccessPermission,)
    serializer_class = None
    document_manager_class = None

    def get_object(self, pk):
        return get_object_or_404(Abiturient, pk=pk)

    def post(self, request, pk, format=None):
        """POST requrest handler

        Validates the data with serializer, got from
        .get_serialzier_class() method and creates document with document
        manager got from .get_document_manager_class() method to create
        document.

        Returns rendered HTML page.
        """

        abiturient = self.get_object(pk)

        serializer_class = self.get_serializer_class()
        document_manager_class = self.get_document_manager_class()

        serializer = serializer_class(data=request.data)

        if not serializer.is_valid():
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

        document_manager = document_manager_class(
            abiturient=abiturient,
            **serializer.validated_data
        )

        rendered_html = document_manager.create()
        return Response({'html': rendered_html}, status=status.HTTP_200_OK)

    def get_serializer_class(self):
        """Returns the data serializer (validator) class. Override this method
        or change .serializer_class class attribute to make the difference
        and add some extra logic.

        Returns .serializer_class
        """

        if not self.serializer_class:
            raise NotImplementedError(
                'You should specify .serializer_class in order to use '
                'DocumentCreateAPIView subclasses'
            )

        return self.serializer_class

    def get_document_manager_class(self):
        """Returns the document manager class. Override this method or change
        .serializer_class class attribute to make the difference
        and add some extra logic.

        Return .docuement_manager_class.
        """

        if not self.document_manager_class:
            raise NotImplementedError(
                'You should specify .document_manager_class in order to use '
                'DocumentCreateAPIView subclasses'
            )

        return self.document_manager_class


class AbiturientContractAPIView(DocumentCreateAPIView):
    """Abiturient Contract Creation APIView"""

    serializer_class = AbiturientContractSerializer
    document_manager_class = AbiturientContractManager
