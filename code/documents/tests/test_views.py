from django.urls import path

from rest_framework.test import APITestCase, URLPatternsTestCase

from rolepermissions.permissions import grant_permission

from abiturients.factories import AbiturientFactory

from admissions.factories import OperatorFactory

from documents.views import AbiturientContractAPIView


class TestAbiturientContractAPIView(APITestCase, URLPatternsTestCase):
    urlpatterns = [
        path(
            'abiturients/<int:pk>/contract/',
            AbiturientContractAPIView.as_view()
        )
    ]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.operator = OperatorFactory()
        cls.abiturient = AbiturientFactory()

        grant_permission(cls.operator, 'can_create_document')

    def test_abiturint_contract_api_view_post(self):
        self.client.force_authenticate(user=self.operator)

        data = {
            'contract_id': 1,
            'start_date': '2019-05-05',
            'end_date': '2020-05-06',
            'price': 99999,
            'study_form': 1,
            'degree': 1,
            'speciality': '[121] Інженерія програмного забезпечення',
            'faculty': 'Факультет Комп\'ютерних Наук і Технологій'
        }

        response = self.client.post(
            f'/abiturients/{self.abiturient.id}/contract/',
            data
        )

        self.assertEqual(response.status_code, 200)
        self.assertIn('html', response.data)
