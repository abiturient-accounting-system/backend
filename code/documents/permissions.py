from rest_framework.permissions import BasePermission

from rolepermissions.checkers import has_permission


class DocumentCreationAccessPermission(BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            return False
        if not has_permission(request.user, 'can_create_document'):
            return False
        return True

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)
