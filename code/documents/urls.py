from django.urls import path

from documents import views


app_name = 'documents'


urlpatterns = [
    path(
        'abiturients/<int:pk>/contract/',
        views.AbiturientContractAPIView.as_view(),
        name='abiturient-contract'
    )
]
